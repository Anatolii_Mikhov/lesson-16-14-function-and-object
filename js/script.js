//script
//script
//----------------------
//Creating an object
//----------------------
var user = {
        userName: 'Anatolii'
        , lastName: 'Mikhov'
        , age: 43
        , work: {
            position: 'Freelance'
            , data_time: '01.08.2017'
            , cost: 500 + ' ' + 'usd'
            , profession: 'Web Coder'
        }
        , music: [
            {
                artist: 'Ajeet'
                , single: 'Sunai'
        }
        , {
                artist: 'Gurujas'
                , single: 'White Sun'
        }
        , {
                artist: 'Snatam'
                , single: 'Liberation door'
        }
    ]
        , access: function () {
            var userName = prompt('Введите имя');
            var lastName = prompt('Введите фамилию');
            if (userName == 'admin' || userName == 'Admin') {
                alert('Hello Admin!');
                title.innerHTML = userName + ' ' + lastName;
            }
            else {
                alert("You don't admin");
                title.innerHTML = userName + ' ' + lastName;
            }
        }
        , ageCheck: function () {
            var ageValidation = prompt('Введите ваш возраст');
            if (ageValidation >= 18) {
                alert('Вход разрешён');
            }
            else {
                alert('Вам вход не разрешён!');
            }
            ageValid.innerHTML = 'Возраст:' + ' ' + ageValidation;
        }
        , addMusic: function () {
            addArtist = prompt('Введите имя артиста');
            addSingle = prompt('Введите название песни');
            var newSong = {
                artist: addArtist
                , single: addSingle
            }
            this.music.push(newSong);
            for (i = 0; i < this.music.length; i++) {
                music.innerHTML = liGenerate(user.music);
            }
        }
        , addWork: function () {
            newPosition = prompt('Введите место работы');
            newData_time = prompt('Введите дату поступления');
            newCost = prompt('Сумма зарплаты');
            newProfession = prompt('Введите должность');
            this.work.position = newPosition;
            this.work.data_time = newData_time;
            this.work.cost = newCost;
            this.work.profession = newProfession;
            work.innerHTML = 'Место работы:' + ' ' + newPosition + '<br>' + 'Начало работы:' + ' ' + newData_time + '<br>' + 'Зарплата:' + ' ' + newCost + '<br>' + 'Специальность:' + ' ' + newProfession;
        }
    }
    //--------------------------------*
    //Object created 
    //--------------------------------*
    //--------------------------------*
    //Variables for inserting into page elements
    //--------------------------------*
var title = document.getElementById('title');
var work = document.getElementById('work');
var music = document.getElementById('music');
//--------------------------------*
//Inserting a value into elements
//--------------------------------*
// Display the first and last name in the element with the class "title"
title.innerHTML = user.userName + ' ' + user.lastName;
ageValid.innerHTML = 'Возраст:' + ' ' +user.age;
//Display information about the user's work
work.innerHTML = 'Место работы:' + ' ' + user.work.position + '<br>' + 'Начало работы:' + ' ' + user.work.data_time + '<br>' + 'Зарплата:' + ' ' + user.work.cost + '<br>' + 'Специальность:' + ' ' + user.work.profession;
//--------------------------------*
//The favorite music
function liGenerate(music) {
    var htmlMusic = ''; // пустой строкой мы убираем значение undefined    
    for (var i = 0; i < music.length; i++) {
        htmlMusic += '<li>';
        htmlMusic += 'Artist:' + ' ' + music[i].artist + '_' + ' ' + 'Single:' + ' ' + music[i].single;
        htmlMusic += '</li>';
    }
    return htmlMusic;
}
music.innerHTML = liGenerate(user.music); // Output artist and song to the list item.